/**
* Représentation abstraite des éléments présents sur les pages (ex: photo)
* @class
* @abstract
*/
class Element{
  /**
  * Crée un élément
  * @constructor
  * @param {Position} position - La position de l'élément
  * @param {Taille} taille - La taille de l'élément
  */
  constructor(position, taille = new Taille(50,100)){
    this.position = position;
    this.plan = 1;
    this.rotation = 0;
    this.taille = taille;
    this.echelleX = 1;
    this.echelleY = 1;
  }

  /**
  * Récupère la valeur du plan
  * @return {number} La valeur du plan
  */
  get plan(){
    return this._plan;
  }

  /**
  * Récupère la position de l'élement
  * @return {Position} La position de l'élement
  */
  get position(){
    return this._position;
  }

  set plan(nouveauPlan){
    this._plan = nouveauPlan;
  }

  /**
  * Assigne une nouvelle position dans l'élement
  * @param nouvellePosition
  */
  set position(nouvellePosition){
    this._position = nouvellePosition;
  }

  /**
  * Permet de changer la position d'un élément
  * @param posX
  * @param posY
  */
  deplacer(posX, posY){
    this.position.posX = posX;
    this.position.posY = posY;
  }

  changerTaille(nouvelleLargeur, nouvelleLongueur){
    this.taille.largeur = nouvelleLargeur;
    this.taille.longueur = nouvelleLongueur;
  }

  /**
   * Modifie l'échelle en X et Y de l'élement
   * @param {number} nouvelleEchelleX
   * @param {number} nouvelleEchelleY
   */
  changerEchelle(nouvelleEchelleX,nouvelleEchelleY){
    this.echelleX = nouvelleEchelleX;
    this.echelleY = nouvelleEchelleY;
  }

  incrementPlanElem(){
    this.plan = this.plan + 1;
  }

  decrementPlanElem(){
    this.plan = this.plan - 1;
  }

  get taille(){
    return this._taille;
  }

  set taille(nouvelTaille){
    this._taille = nouvelTaille;
  }

  get rotation(){
    return this._rotation;
  }

  set rotation(nouvelRotation){
    this._rotation = nouvelRotation;
  }

  get echelleX(){
    return this._echelleX;
  }

  set echelleX(nouvelleEchelleX){
    this._echelleX = nouvelleEchelleX;
  }

  get echelleY(){
    return this._echelleY;
  }

  set echelleY(nouvelleEchelleY){
    this._echelleY = nouvelleEchelleY;
  }
}
