// Initialisation de la banque d'images
function getGallery(){
  var photos =
  [ "../Vue/data/demo/IMG_20180629_143702.jpg",
  "../Vue/data/demo/IMG_20180629_143710.jpg",
  "../Vue/data/demo/IMG_20180629_150148.jpg",
  "../Vue/data/demo/IMG_20180629_152340.jpg",
  "../Vue/data/demo/IMG_20180629_153034~2.jpg",
  "../Vue/data/demo/IMG_20190122_133131151.jpg",
  "../Vue/data/demo/IMG_20190122_133224334.jpg",
  "../Vue/data/demo/IMG_20190122_133504876.jpg",
  /* A enlever pour la vidéo avec JP */
  "../Vue/data/images_jpg/1.jpg",
  "../Vue/data/images_jpg/2.jpg",
  "../Vue/data/images_jpg/3.jpg",
  "../Vue/data/images_jpg/4.jpg",
  "../Vue/data/images_jpg/5.jpg",
  "../Vue/data/images_jpg/6.jpg",
  "../Vue/data/images_jpg/7.png",
  "../Vue/data/images_jpg/8.jpg",
  "../Vue/data/images_jpg/9.jpg",
  "../Vue/data/images_jpg/10.jpg",
  "../Vue/data/images_png/valeur_ajoute.png",
  "../Vue/data/images_png/photoweb.png"];

  return photos;
}
