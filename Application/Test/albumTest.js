// Création de l'album de test :
var album = new AlbumPhoto("TitreTest",27);

var tab = ["../Vue/data/images_png/Fond-12.png","../Vue/data/images_png/Fond-12.png"];

album.themeGeneral = new ThemeGeneral("Theme De Test", tab);
album.appliquerTheme();

//album.nouvelElementAPage(1,new Position(60,270),"text");
//album.pageAtPos(1).elements[0].chaine = "Par l'équipe OCAPI";


// Pour calculer le ratio permettant d'obtenir la bonne taille des éléments au bon endroit
var idealWidth = album.taille.largeur;
var idealHeight = album.taille.longueur;

// Ajout d'éléments pour la démonstration, décommenter afin d'appliquer cette configuration


// album.nouvelElementAPage(1,new Position(0,20),"img");
// album.pageAtPos(1).elements[0].source = "../Vue/data/images_jpg/titre.png";
// album.pageAtPos(1).elements[0].echelleX =0.96;
// album.pageAtPos(1).elements[0].echelleY =0.96;
// album.pageAtPos(1).elements[0].taille.longueur =45.12;
// album.pageAtPos(1).elements[0].taille.largeur =239.6514;
//
// album.nouvelElementAPage(1,new Position(2.52,89.2),"img");
// album.pageAtPos(1).elements[1].source = "../Vue/data/images_jpg/road.jpg";
// album.pageAtPos(1).elements[1].echelleX =0.4648;
// album.pageAtPos(1).elements[1].echelleY =0.4648;
// album.pageAtPos(1).elements[1].taille.longueur =288.8266;
// album.pageAtPos(1).elements[1].taille.largeur =460.1307;
//
// album.nouvelElementAPage(3,new Position(-64,-59),"img");
// album.pageAtPos(3).elements[0].source = "../Vue/data/images_jpg/polaroid.png";
// album.pageAtPos(3).elements[0].echelleX =0.25;
// album.pageAtPos(3).elements[0].echelleY =0.25;
// album.pageAtPos(3).elements[0].taille.longueur =1848.5;
// album.pageAtPos(3).elements[0].taille.largeur =1388.061;
//
// album.nouvelElementAPage(4,new Position(5,-45),"img");
// album.pageAtPos(4).elements[0].source = "../Vue/data/images_jpg/pont.png";
// album.pageAtPos(4).elements[0].echelleX =0.15;
// album.pageAtPos(4).elements[0].echelleY =0.15;
// album.pageAtPos(4).elements[0].taille.longueur =1848.5;
// album.pageAtPos(4).elements[0].taille.largeur =1388.061;
//
// album.nouvelElementAPage(4,new Position(5,100),"img");
// album.pageAtPos(4).elements[1].source = "../Vue/data/images_jpg/canoe.png";
// album.pageAtPos(4).elements[1].echelleX =0.15;
// album.pageAtPos(4).elements[1].echelleY =0.15;
// album.pageAtPos(4).elements[1].taille.longueur =1848.5;
// album.pageAtPos(4).elements[1].taille.largeur =1388.061;
//
// album.nouvelElementAPage(5,new Position(0,0),"img");
// album.pageAtPos(5).elements[0].source = "../Vue/data/images_jpg/timbre1.png";
// album.pageAtPos(5).elements[0].echelleX =0.75;
// album.pageAtPos(5).elements[0].echelleY =0.75;
// album.pageAtPos(5).elements[0].taille.longueur =176;
// album.pageAtPos(5).elements[0].taille.largeur =215.686;
//
// album.nouvelElementAPage(5,new Position(0,150),"img");
// album.pageAtPos(5).elements[1].source = "../Vue/data/images_jpg/timbre2.png";
// album.pageAtPos(5).elements[1].echelleX = 0.9;
// album.pageAtPos(5).elements[1].echelleY = 0.9;
// album.pageAtPos(5).elements[1].taille.longueur =176;
// album.pageAtPos(5).elements[1].taille.largeur =215.686;
//
// album.nouvelElementAPage(5,new Position(70,50),"img");
// album.pageAtPos(5).elements[2].source = "../Vue/data/images_jpg/timbre3.png";
// album.pageAtPos(5).elements[2].echelleX =0.7;
// album.pageAtPos(5).elements[2].echelleY =0.7;
// album.pageAtPos(5).elements[2].taille.longueur =176;
// album.pageAtPos(5).elements[2].taille.largeur =215.686;
//
// album.appliquerCouleurPolice("black");
